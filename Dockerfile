FROM node:latest

WORKDIR /docker-api

ADD . /docker-api

RUN npm install --production
#               --only=production

EXPOSE 3000

CMD ["npm", "run", "start"]
