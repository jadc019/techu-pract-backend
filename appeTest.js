var express = require('express');
var usersJSON = require('./users.json');
var req
var port = process.env.PORT || 3000;

const app = express();
const bodyParser = require('body-parser');
const url = require('url');
const querystring = require('querystring');

app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

var urlBase = "/apiperu/v1";

app.get(urlBase + '/users', function (req, res) {
  res.status(200).send(usersJSON);
});

app.get(urlBase + '/users/:id1/:id2', function (req, res) {
    console.log(req.params);
    console.log(req.query);
    res.status(200).send({});
});

app.get(urlBase + '/users/:id', function (req, res) {
  var users = usersJSON.filter( item => item.user_id == req.params.id );
  if (!users.length)
    res.status(404).send({"msje":"usuario no encontrado"});
  else
    res.status(200).send(users[0]);
});

app.post(urlBase + '/users', function (req, res) {
  var newUser =  {
        "user_id": usersJSON[usersJSON.length -1].user_id + 1,
        "first_name": req.body.first_name,
        "last_name": req.body.last_name,
        "email": req.body.email,
        "password": req.body.password
    }
  usersJSON.push(newUser);
  res.status(201).send(newUser);
});

app.put(urlBase + '/users/:id', function (req, res) {
  console.log(req.params.id );
  var index = usersJSON.map( item => item.user_id).indexOf(parseInt(req.params.id));
  if (index < 0) {
    res.status(404).send({"msje":"usuario no encontrado"});
    return;
  }

  var oldUser = usersJSON[index];
  var newUser = Object.assign(oldUser, req.body);
  usersJSON.splice(index, 1, newUser);
  res.status(200).send(newUser);
});

app.delete(urlBase + '/users/:id', function (req, res) {
  var index = usersJSON.map( item => item.user_id).indexOf(parseInt(req.params.id));
  if(index < 0){
    res.status(404).send({"msje":"usuario no encontrado"});
    return;
  }

  var user = usersJSON[index];
  usersJSON.splice(index, 1);
  res.status(200).send(user);
});

app.listen(port, function () {
  console.log('Example app listening on port 3000!');
});
