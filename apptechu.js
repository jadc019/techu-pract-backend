const port = process.env.PORT || 3000;
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const requestJson = require('request-json');

app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

var urlMlabRaiz = "https://api.mlab.com/api/1/databases/apitechudb/collections"
var apiKey = "apiKey=JqRQthDZEhi4K_IiKXnNvRhm2-fZd3UM"
var clienteMlab;

var urlBase = "/mlaptest/v1";

app.get(urlBase + '/users', function (req, res) {
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/user?" + apiKey)
    clienteMlab.get('', function(err, resM, body) {
      if (!err) {
        res.send(body)
      }
    })
});

app.get(urlBase + '/users/:id', function(req, res) {
    var id = req.params.id
    var query = 'q={"user_id":' + id + '}&f={"_id":0, "password":0}'
    clienteMlab = requestJson.createClient(urlMlabRaiz + "/user?" + query + "&l=1&" + apiKey);
    clienteMlab.get('', function(err, resM, body) {
      if (!err) {
        if (body.length > 0)
          res.send(body[0])
          //res.send({"nombre":body[0].nombre, "apellidos":body[0].apellidos})
        else {
          res.status(404).send('Usuario no encontrado')
        }
      }
    })
})

app.post(urlBase + '/users', function (req, res) {
  console.log(req.body);
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/user?" + apiKey);
  clienteMlab.post('', req.body, function(err, resM, body) {
  //  if (!err) {
      console.log(body);
        res.send(body)
        //res.send({"nombre":body[0].nombre, "apellidos":body[0].apellidos})
  //  }  else {
  //    res.status(503).send('Error interno')
  //  }
  })
});

//PUT of user
app.put(urlBase + '/users/:id', function(req, res) {
  console.log(req.body);
  var id = req.params.id;
  var queryString = 'q={"user_id":' + id + '}&';
  var cambio = {"$set": req.body };
  console.log(cambio);
  clienteMlab = requestJson.createClient(urlMlabRaiz + '/user?' + queryString + apiKey);
  clienteMlab.put('', cambio, function(error, resMLab, body) {
    res.send(body);
  });
});

//DELETE user with id
app.delete(urlBase + "/users/:id", function(req, res){
    console.log("request.params.id: " + req.params.id);
    var id = req.params.id;
    var queryStringID = 'q={"user_id":' + id + '}&';
    clienteMlab = requestJson.createClient(urlMlabRaiz + "/user?" + queryStringID + apiKey);
    clienteMlab.delete('', function(error, respuestaMLab, body){
        res.send(body);
    });
});

/*
//PUT of user
app.put(urlBase + '/users/:id', function(req, res) {
  var id = req.params.id
  var query = 'q={"user_id":' + id + '}'
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/user?" + query + "&l=1&" + apiKey)
  clienteMlab.get('', function(error, respuestaMLab , body) {
     if (!body.length){
       res.status(404).send({"msje":"usuario no encontrado"});
       return;
     }

     var cambio = {"$set": req.body};

     clienteMlab = requestJson.createClient(urlMlabRaiz + '/user/' + body[0]._id.$oid +'?' + apiKey);
     clienteMlab.put('', cambio, function(error, respuestaMLab, bodyMlab) {
       console.log("body:"+ bodyMlab);
       res.send(body);
      });
 });
});

//DELETE user with id
app.delete(urlBase + "/users/:id", function(req, res){
    console.log("entra al DELETE");
    console.log("request.params.id: " + req.params.id);
    var id = req.params.id;
    var queryStringID = 'q={"id":' + id + '}&';
    console.log(urlMlabRaiz + 'user?' + queryStringID + apiKey);
    var httpClient = requestJson.createClient(baseMLabURL);
    httpClient.get('user?' +  queryStringID + apiKey,
      function(error, respuestaMLab, body){
        var respuesta = body[0];
        console.log("body delete:"+ respuesta);
        httpClient.delete(urlMlabRaiz + "user/" + respuesta._id.$oid +'?'+ apiKey,
          function(error, respuestaMLab,body){
            res.send(body);
        });
      });
  });
*/

//LOGIN user with correo y password
app.post(urlBase + "/login", function(req, res){
  console.log("LOGIN...");
  var query = "q={'email':'" + req.body.email + "', 'password':'" + req.body.password + "'}";
  console.log(query);
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/user?" + query + "&l=1&" + apiKey);
  clienteMlab.get('', function(error, respuestaMLab, body){
    if(!error){
      console.log(body);
      if(body.length){
        var cambio = {"$set": {'logged':true} };
        console.log(cambio);
        clienteMlab = requestJson.createClient(urlMlabRaiz + '/user/' + body[0]._id.$oid + '?' + apiKey);
        clienteMlab.put('', cambio, function(errorPut, resMLabPut, bodyPut) {
          res.send({'msje':'OK', 'correo':query.email, 'body':bodyPut[0]});
        });
      } else {
        res.status(404).send({"msje":"Email y/o passwrod incorrectos"});
      }
    } else {
      res.status(500).send({"msje":"Ocurrio un error interno"});
    }
  });
});

//LOGOUT user with correo
app.post(urlBase + "/logout", function(req, res){
  console.log("LOGIN...");
  var query = "q={'email':'" + req.body.email + "', 'logged':true}";
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/user?" + query + "&l=1&" + apiKey);
  clienteMlab.get('', function(error, respuestaMLab, body){
    if(!error){
      if(body.length){
        var cambio = {"$unset": {'logged':true} };
        console.log(cambio);
        clienteMlab = requestJson.createClient(urlMlabRaiz + '/user/' + body[0]._id.$oid + '?' + apiKey);
        clienteMlab.put('', cambio, function(errorPut, resMLabPut, bodyPut) {
          res.send({'msje':'OK', 'correo':query.email, 'body':bodyPut[0]});
        });
      } else {
        res.status(404).send({"msje":"Email no vàlido"});
      }
    } else {
      res.status(500).send({"msje":"Ocurrio un error interno"});
    }
  });
});

app.listen(port, function () {
  console.log('Example app listening on port 3000!');
});
